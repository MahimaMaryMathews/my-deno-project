import ProductRepository from "../repositories/ProductRepository.ts";
import Product from "../interfaces/Iproduct.ts";

class ProductService {
  constructor() {}

  private products: Product[] = [];

  readonly productRepository = new ProductRepository();

  getProduct = async (productID: string) => {
    return await this.productRepository.findOne(productID);
  };

  getProducts = async () => {
    return await this.productRepository.find();
  };

  createProduct = async (product: Product) => {
    return await this.productRepository.insertOne(product);
  };

  updateProduct = async (productID: string, product: Product) => {
    const v = await this.productRepository.update(productID, product);
    return v;
  };

  deleteProduct = async (productID: string) => {
    return await this.productRepository.delete(productID);
  };
}

export default ProductService;
