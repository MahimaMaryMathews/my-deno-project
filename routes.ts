import { Router } from "https://deno.land/x/oak/mod.ts";

import {
  getProduct,
  getProducts,
  createProduct,
  updateProduct,
  deleteProduct,
} from "./controllers/productController.ts";

const router = new Router();

router
  .get("/products/:id", getProduct)
  .get("/products", getProducts)
  .post("/products", createProduct)
  .put("/products/:id", updateProduct)
  .delete("/products/:id", deleteProduct);

export default router;
