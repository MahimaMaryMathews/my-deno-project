export default interface Product {
  readonly id?: string;
  readonly name: string;
  readonly organisation: string;
  readonly islive: boolean;
  readonly description: string;
  readonly address: string;
  readonly price: number;
}
