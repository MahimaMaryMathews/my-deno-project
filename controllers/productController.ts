import { RouterContext } from "https://deno.land/x/oak/mod.ts";

import ProductService from "../services/ProductService.ts";

const productService = new ProductService();

export const getProduct = async (context: RouterContext) => {
  const { id } = context.params as { id: string };
  context.response.body = await productService.getProduct(id);
};

export const getProducts = async (context: RouterContext) => {
  context.response.body = await productService.getProducts();
};

export const createProduct = async (context: RouterContext) => {
  const { request, response } = context;

  if (!request.hasBody) {
    response.status = 400;
    response.body = { msg: "Invalid product data" };
    return;
  }

  const {
value: {
    name,
    organisation,
    islive,
    description,
    address,
    price
  },
  } = await request.body();

  const productId = await productService.createProduct(
    {
      name,
      organisation,
      islive,
      description,
      address,
      price,
    },
  );
  response.body = { msg: "Product Created", productId };
};

export const updateProduct = async (context: RouterContext) => {
  const { id } = context.params as { id: string };
  const { request, response } = context;

  if (
    (!request.hasBody) ||
    (context.request.headers.get("content-type") !== "application/json")
  ) {
    response.status = 400;
    response.body = { msg: "Invalid product data/body" };
    return;
  }

  const {
value: {
        name,
        organisation,
        islive,
        description,
        address,
        price
      },
  } = await request.body();

  context.response.body = await productService.updateProduct(id, {
    name,
    organisation,
    islive,
    description,
    address,
    price,
  });
};

export const deleteProduct = async (context: RouterContext) => {
  const { id } = context.params as { id: string };
  context.response.body = await productService.deleteProduct(id);
};
