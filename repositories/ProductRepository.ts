import db from "../db/mongo.ts";
import Product from "../interfaces/Iproduct.ts";

class ProductRepository {
  constructor() {}

  readonly productCollection = db.collection("products");

  async findOne(id: String) {
    const products = await this.productCollection.find({ _id: { "$oid": id } });
    return products;
  }

  async find() {
    const products = await this.productCollection.find();
    return products;
  }

  async insertOne(product: Product) {
    const { $oid } = await this.productCollection.insertOne(product);
    return $oid;
  }

  async update(id: String, product: {}) {
    await this.productCollection.updateOne(
      { _id: { "$oid": id } },
      { $set: product },
    );
    return "success";
  }

  async delete(id: String) {
    const products = await this.productCollection.deleteOne(
      { _id: { "$oid": id } },
    );
    return products;
  }
}

export default ProductRepository;
